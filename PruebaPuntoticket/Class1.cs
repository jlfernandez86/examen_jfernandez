﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using log4net;
using Quartz;

namespace ExamPuntoticket
{
    public class Class1 : IJob
    {
        private readonly ILog _log;

        public Class1(ILog log)
        {
            _log = log;
        }

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("Start process file");

            var l = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "code.txt"));
            var numerated = new List<string>();
            var notNumerated = new List<string>();
            var typeGroup = new Dictionary<string, int>();
            string[] lineas_leidas = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "code.txt"));

            //Lineas con tílde erróneos

            _log.Debug("Read code types");
            foreach (var s in l)
            {
                var data = s.Split(',');
                if (data[1] == "Cancha General")
                {
                    notNumerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Cancha General"))
                    {
                        typeGroup["Cancha General"] = typeGroup["Cancha General"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Cancha General", 1);
                    }
                }
                if (data[1].StartsWith("Galer"))
                {
                    notNumerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Galería"))
                    {
                        typeGroup["Galería"] = typeGroup["Galería"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Galería", 1);
                    }
                }
                if (data[1] == "Vip")
                {
                    numerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Vip"))
                    {
                        typeGroup["Vip"] = typeGroup["Vip"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Vip", 1);
                    }
                }
                if (data[1] == "Palco")
                {
                    numerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Palco"))
                    {
                        typeGroup["Palco"] = typeGroup["Palco"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Palco", 1);
                    }
                }
            }

            //JLF
            var listado = lineas_leidas.Select(x => x.Split(',')).Select(it => new { number = it[0], type = it[1] });
            _log.Debug("Save file numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(),"numerated_"+DateTime.Now.Ticks+".txt")))
            {
                foreach (var i in listado.Where(i => new String[] { "Vip", "Palco" }.Contains(i.type)))
                {
                    file.WriteLine(i.number);
                }
            }

            _log.Debug("Save file not numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "notnumerated_" + DateTime.Now.Ticks + ".txt")))
            {
                foreach (var i in listado.Where(i => new String[] { "Cancha General", "Galería", "Galer�a" }.Contains(i.type)))
                {
                    file.WriteLine(i.number);
                }
            }


            _log.Debug("Save file group for types");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "typegroup_" + DateTime.Now.Ticks + ".txt")))
            {
                //JFC
                int total_general_cancha = listado.Where(i => i.type.Equals("Cancha General")).Count();
                int total_galeria = listado.Where(i => i.type.Equals("Galería") || i.type.StartsWith("Galer")).Count();
                int total_palco = listado.Where(i => i.type.Equals("Palco")).Count();
                int total_vip = listado.Where(i => i.type.Equals("Vip")).Count();

                file.WriteLine(total_general_cancha > 0 ? "Cancha General: " + total_general_cancha : null);
                file.WriteLine(total_palco > 0 ? "Palco: " + total_palco : null);
                file.WriteLine(total_vip > 0 ? "Vip: " + total_vip : null);
                file.WriteLine(total_galeria > 0 ? "Galería: " + total_galeria : null);

                /*foreach (var line in typeGroup)
                {
                    file.WriteLine(line);
                }*/

            }
        }
    }
}